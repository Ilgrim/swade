# 0.7.X Compatability To-Do list

- [x] `ActorSheet._onDrop` factored out -> figure out what that means and fix vehicle sheet if necessary -> https://discordapp.com/channels/170995199584108546/729084554119348245/748602171549024290
- [ ] Adjust new Dice api to label dice and possibly make explosion syntax look better
  - Update: Gotta wait until it can support spaces
- [ ] Add basics for active effects API
- [x] Check Sheet item creation dialogs
- [x] Fix roll dialogs
- [x] Fix item creation dialog
- [x] Fix macro creation drag handler
